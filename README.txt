
-- SUMMARY --

  The module allows author of content to use bootstrap style in a simpler way:

  - Automatically applying responsive style to all images.

  - Using Glyphicons (http://getbootstrap.com/components/) in a simpler way.
    
    For example:
    Using <g-search>, instead of <span class="glyphicon glyphicon-search"></span>
    Using <g-plus>, instead of <span class="glyphicon glyphicon-plus"></span>

-- REQUIREMENTS --

  Bootstrap library.
  simplehtmldom

-- INSTALLATION --

  Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

  Apply the bootstrap file to some formats (Content authoring -> Text formats).

